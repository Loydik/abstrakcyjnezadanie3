﻿using System.Collections;
using System.Collections.Generic;

namespace TripleListNamespace
{
    public class TripleList<T> : IEnumerable<T>
    {
        #region Private Members

        private T _value;
        private bool _valueSet;
        private readonly bool _isMiddleElement;
        private TripleList<T> _middleElement;

        #endregion


        #region Properties

        public TripleList<T> Parent { get; set; }

        public T Value
        {
            get { return _value; }
            set
            {
                _value = value;
                _valueSet = true;
            }
        }
        public TripleList<T> PreviousElement { get; set; }

        public TripleList<T> MiddleElement {

            get
            {
                if (_isMiddleElement)
                {
                    return Parent;
                }

                return _middleElement;
            }

            set { _middleElement = value; }
        }

        public TripleList<T> NextElement { get; set; }

        #endregion


        #region Construction

        public TripleList()
        {

        }

        public TripleList(TripleList<T> parent, bool isMiddle, T value)
        {
            Parent = parent;
            _isMiddleElement = isMiddle;
            Value = value;
        }

        #endregion


        #region Public Methods

        public void Add(T element)
        {
            if (!_valueSet)
            {
                Value = element;
            }

            else
            {
                if (MiddleElement == null && _isMiddleElement == false)
                {
                    MiddleElement = new TripleList<T>(this, true, element);
                }

                else if (MiddleElement == null && _isMiddleElement)
                {
                    MiddleElement = Parent.MiddleElement;
                }

                else if (NextElement == null)
                {
                    NextElement = new TripleList<T>(this, false, element);
                    NextElement.PreviousElement = new TripleList<T>(this, false, Value);
                }

                else
                {
                    NextElement?.Add(element);
                }
            }

        }

        public void Add(TripleList<T> element)
        {
            if (!_valueSet)
            {
                Value = element.Value;
                MiddleElement = element.MiddleElement;
                NextElement = element.NextElement;
            }

            else
            {
                if (MiddleElement == null && _isMiddleElement == false)
                {
                    MiddleElement = new TripleList<T>(this, true, element.Value);
                    NextElement = element.MiddleElement;
                    NextElement.MiddleElement = element.NextElement;
                }

                else if (MiddleElement == null && _isMiddleElement)
                {
                    MiddleElement = element.MiddleElement;
                    NextElement = element.MiddleElement;
                    NextElement.MiddleElement = element.NextElement;
                }

                else if (NextElement == null)
                {
                    NextElement = new TripleList<T>(this, false, element.Value)
                    {
                        MiddleElement = element.MiddleElement,
                        NextElement = element.NextElement,
                        PreviousElement = new TripleList<T>(this, false, Value)
                    };
                }

                else
                {
                    NextElement?.Add(element);
                }
            }
        }

        #endregion


        #region Private Members

        private List<T> GetAllChildren()
        {
            var result = new List<T>();

            if (MiddleElement != null && MiddleElement._valueSet)
            {
                result.Add(MiddleElement.Value);
            }

            if (NextElement != null && NextElement._valueSet)
            {
                result.Add(NextElement.Value);
            }

            if (NextElement?.MiddleElement != null)
            {
                result.AddRange(NextElement.GetAllChildren());
            }

            return result;
        }

        private List<T> GetAllElements(TripleList<T> start)
        {
            var result = new List<T>();

            if (_valueSet)
            {
                result.Add(Value);
            }

            result.AddRange(GetAllChildren());

            return result;
        }

        #endregion


        #region IEnumerator Members

        public IEnumerator<T> GetEnumerator()
        {
            var result = GetAllElements(this);

            foreach (var element in result)
            {
                yield return element;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

    }

}
